A new repository for a new organization for a fledgling freelance company...

My mission statement has changed enormously since I created this repository.  I would now like to perform region-specific community outreach with respect to drug epidemics by building webpages for the purpose of sharing information - hopefully as a part of the Montana Meth Project in a branch capacity.

My other main focus is creating ways to fix the 'holes' in the internet that allow black-hat hackers to appropriate the private information that allows for mass destruction of ordinary people's credit histories', et cetera.  

Hacktivism-related DDOS is one thing, wanton playing with the lives of innocents is quite another.